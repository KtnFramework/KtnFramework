# search for the name of the OS
if [ -f /etc/os-release ]; then
    # on systems with freedesktop.org and systemd, the distro name can be found in /etc/os-release
    source /etc/os-release
    OS=$PRETTY_NAME
    VER=$VERSION_ID
fi

echo "Host operating system: $OS."
echo "Installing dependencies..."

case $OS in
openSUSE*)
    # build tools
    sudo zypper install --no-recommends cmake gcc gcc-c++
    # library dependencies
    sudo zypper install --no-recommends \
    glm-devel \
    glu-devel \
    libbullet-devel \
    libepoxy-devel \
    libglfw-devel \
    libvorbis-devel \
    ninja \
    portaudio-devel \
    rapidjson-devel \
    vulkan-devel \
    vulkan-validationlayers-devel
;;

Ubuntu*)
    sudo apt install \
    cmake \
    gcovr \
    libepoxy-dev \
    libglfw3-dev \
    libglm-dev \
    libvorbis-dev \
    libvulkan-dev \
    ninja \
    portaudio19-dev \
    --no-install-recommends
;;

*)
    echo "This script does not support setting up the development environment in \"$OS\" (yet)."
    exit 1
    ;;
esac

uname -a || { exit 1; }
cmake --version || { exit 1; }
g++ --version || { exit 1; }

git submodule init
git submodule update

function download_or_update {
    if [ -d $1 ]; then
        echo "Updating $1..."
        cd $1
        git pull
        if [ $? -ne 0 ]; then
            echo "Error updating $1! Downloading instead..."
            cd ..
            rm -rf $1
            download_or_update $1 $2
        else
            cd ..
        fi
    else
        echo "Downloading $1..."
        git clone $2 || {
            echo "Error downloading $1!"
            exit 1
        }
        echo "Dependency \"$1\" from $2 downloaded."
    fi
}

echo "--------------------------------------------------------------------------------"
download_or_update KtnEngine https://gitlab.com/KtnFramework/KtnEngine/KtnEngine.git \
&& cd KtnEngine \
&& ./ci-scripts/prebuild.sh \
&& cd ..

echo "--------------------------------------------------------------------------------"
download_or_update examples https://gitlab.com/KtnFramework/KtnEngine/examples.git

echo "--------------------------------------------------------------------------------"
download_or_update resources https://gitlab.com/KtnFramework/KtnEngine/resources.git

