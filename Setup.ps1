git submodule init
git submodule update
git submodule foreach --recursive "git checkout master && git pull --rebase"

$temp = Get-Location

if ($?) { Set-Location KtnEngine }

if ($?) { ./ci-scripts/prebuild.ps1 }
else {
    Set-Location $temp
    exit 1
}
if ($?) { Write-Output "KtnFramework Setup complete." }
else {
    Set-Location $temp
    exit 1
}

Set-Location $temp